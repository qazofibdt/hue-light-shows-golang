package hue

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"sync"
)

type client struct {
	httpClient http.Client
	apiPath    string
}

func (c *client) put(query string, body interface{}, wg *sync.WaitGroup) {
	jsonBody, err := json.Marshal(body)
	if err != nil {
		panic(err)
	}
	os.Stdout.Write(jsonBody)
	fmt.Println()
	jsonBodyReader := bytes.NewReader(jsonBody)
	req, err := http.NewRequest(http.MethodPut, query, jsonBodyReader)
	if err != nil {
		panic(err)
	}
	wg.Add(1)
	go func() {
		defer wg.Done()
		resp, _ := c.httpClient.Do(req)
		fmt.Println(resp.Status)
	}()
}

type LightID int
type GroupID int

type lightStateParams struct {
	On             *bool       `json:"on,omitempty"`
	Bri            *uint8      `json:"bri,omitempty"`
	Hue            *uint16     `json:"hue,omitempty"`
	Sat            *uint8      `json:"sat,omitempty"`
	Xy             *[2]float32 `json:"xy,omitempty"`
	Ct             *uint16     `json:"ct,omitempty"`
	TransitionTime uint16      `json:"transitiontime"`
}

func (c *client) setLightState(id LightID, params lightStateParams, wg *sync.WaitGroup) {
	c.put(c.apiPath+fmt.Sprintf("lights/%v/state", id), params, wg)
}

func (c *client) setGroupState(id GroupID, params lightStateParams, wg *sync.WaitGroup) {
	c.put(c.apiPath+fmt.Sprintf("groups/%v/action", id), params, wg)
}
