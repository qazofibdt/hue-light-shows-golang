package hue

import (
	"fmt"
	"net/http"
	"sync"
	"time"
)

type LightSetter interface {
	SetOn(on bool)
	SetBrightness(brightness uint8)

	SetHue(hue uint16)
	// 0 to 254
	SetSaturation(saturation uint8)

	SetXY(x float32, y float32)

	// Units are "reciprocal megakelvin" (mirek). Range is 153 mirek (6500K) to 500 mirek (2000K)
	SetTemperature(temperature uint16)
}

type Controller interface {
	GetLight(id LightID) LightSetter
	GetLights(id ...LightID) LightSetter
	GetGroup(id GroupID) LightSetter
	// Wait for all network requests to finish
	Wait()
}

type controller struct {
	client *client
	wg     *sync.WaitGroup
}

func (c controller) GetLight(id LightID) LightSetter {
	return light{c.client, id, c.wg}
}

func (c controller) GetLights(ids ...LightID) LightSetter {
	lights := lights{}
	for _, id := range ids {
		lights = append(lights, c.GetLight(id))
	}
	return lights
}

func (c controller) GetGroup(id GroupID) LightSetter {
	return group{c.client, id, c.wg}
}

func (c controller) Wait() {
	c.wg.Wait()
}

func MakeController(ip string, auth string) Controller {
	apiPath := fmt.Sprintf("http://%v/api/%v/", ip, auth)
	httpClient := http.Client{Timeout: 1 * time.Second}
	client := client{httpClient, apiPath}
	wg := sync.WaitGroup{}
	return controller{&client, &wg}
}

type light struct {
	client *client
	id     LightID
	wg     *sync.WaitGroup
}

func (l light) SetOn(on bool) {
	l.client.setLightState(l.id, lightStateParams{On: &on}, l.wg)
}

func (l light) SetBrightness(brightness uint8) {
	l.client.setLightState(l.id, lightStateParams{Bri: &brightness}, l.wg)
}

func (l light) SetHue(hue uint16) {
	l.client.setLightState(l.id, lightStateParams{Hue: &hue}, l.wg)
}

func (l light) SetSaturation(saturation uint8) {
	l.client.setLightState(l.id, lightStateParams{Sat: &saturation}, l.wg)
}

func (l light) SetXY(x, y float32) {
	l.client.setLightState(l.id, lightStateParams{Xy: &[2]float32{x, y}}, l.wg)
}

func (l light) SetTemperature(temperature uint16) {
	l.client.setLightState(l.id, lightStateParams{Ct: &temperature}, l.wg)
}

type lights []LightSetter

func (ls lights) SetOn(on bool) {
	for _, light := range ls {
		light.SetOn(on)
	}
}

func (ls lights) SetBrightness(brightness uint8) {
	for _, light := range ls {
		light.SetBrightness(brightness)
	}
}

func (ls lights) SetHue(hue uint16) {
	for _, light := range ls {
		light.SetHue(hue)
	}
}

func (ls lights) SetSaturation(saturation uint8) {
	for _, light := range ls {
		light.SetSaturation(saturation)
	}
}

func (ls lights) SetXY(x, y float32) {
	for _, light := range ls {
		light.SetXY(x, y)
	}
}

func (ls lights) SetTemperature(temperature uint16) {
	for _, light := range ls {
		light.SetTemperature(temperature)
	}
}

type group struct {
	client *client
	id     GroupID
	wg     *sync.WaitGroup
}

func (g group) SetOn(on bool) {
	g.client.setGroupState(g.id, lightStateParams{On: &on}, g.wg)
}

func (g group) SetBrightness(brightness uint8) {
	g.client.setGroupState(g.id, lightStateParams{Bri: &brightness}, g.wg)
}

func (g group) SetHue(hue uint16) {
	g.client.setGroupState(g.id, lightStateParams{Hue: &hue}, g.wg)
}

func (g group) SetSaturation(saturation uint8) {
	g.client.setGroupState(g.id, lightStateParams{Sat: &saturation}, g.wg)
}

func (g group) SetXY(x, y float32) {
	g.client.setGroupState(g.id, lightStateParams{Xy: &[2]float32{x, y}}, g.wg)
}

func (g group) SetTemperature(temperature uint16) {
	g.client.setGroupState(g.id, lightStateParams{Ct: &temperature}, g.wg)
}
