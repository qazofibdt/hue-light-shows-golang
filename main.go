package main

import (
	"antibacterial/lightshow/hue"
	"fmt"
	"time"
	"io/ioutil"
	"strings"
)

func getAuth() (ip string, auth string) {
	dat, err := ioutil.ReadFile("../hue-auth/auth.txt")
	if err != nil {
		panic(err)
	}

	datStr := string(dat)
	// For some reason ioutil.ReadFile returns a newline even if there isn't one
	datStr = strings.TrimSuffix(datStr, "\n")

	tokens := strings.Split(datStr, ",")

	if len(tokens) != 2 {
		panic("Malformed auth file")
	}

	return tokens[0], tokens[1]
}

func main() {
	fmt.Println("begin")

	ip, auth := getAuth()

	controller := hue.MakeController(ip, auth)
	scheduler := sequentialSleepScheduler{}

	controller.GetGroup(GroupAll).SetOn(false)
	scheduler.sleep(1 * time.Second)
	controller.GetGroup(GroupAll).SetOn(true)
	scheduler.sleep(1 * time.Second)
	controller.GetGroup(GroupAll).SetOn(false)
	scheduler.sleep(1 * time.Second)
	controller.GetGroup(GroupAll).SetOn(true)
	scheduler.sleep(1 * time.Second)
	controller.GetGroup(GroupAll).SetOn(false)
	scheduler.sleep(1 * time.Second)
	controller.GetGroup(GroupAll).SetOn(true)
	scheduler.sleep(1 * time.Second)

	controller.GetLight(LightKitchenNE).SetTemperature(TemperatureDefaultWhite)

	controller.Wait()
}
