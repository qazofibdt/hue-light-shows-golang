package main

const (
	LightDoorS     = 1
	LightDoorN     = 2
	LightTableNW   = 3
	LightTableNE   = 4
	LightKitchenNW = 5
	LightKitchenNE = 6
	LightKitchenSW = 7
	LightKitchenSE = 8
	LightHallwayW  = 9
	LightHallwayE  = 10
	LightTableS    = 11
	LightComputer  = 12
)

const (
	GroupAll        = 0
	GroupLivingRoom = 1
	GroupKitchen    = 2
	GroupHallway    = 3
)

const (
	HueRed    = 0
	HueYellow = 12750
	HueGreen  = 21845
	HueBlue   = 43690
	HuePink   = 56100
)

const (
	TemperatureDefaultWhite = 370
)
