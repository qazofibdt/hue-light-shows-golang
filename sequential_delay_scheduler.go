package main

import (
	"time"
)

type sequentialSleepScheduler struct {
	anchorTime time.Time
}

func (s *sequentialSleepScheduler) anchor() {
	s.anchorTime = time.Now()
}

func (s *sequentialSleepScheduler) sleep(d time.Duration) {
	if s.anchorTime.IsZero() {
		s.anchor()
	}

	actual := time.Until(s.anchorTime.Add(d))
	s.anchorTime = s.anchorTime.Add(d)
	time.Sleep(actual)
}
